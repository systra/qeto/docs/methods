# Documentation for SDS.

[https://systra.gitlab.io/qeto/docs/methods/](https://systra.gitlab.io/qeto/docs/methods/)

This [VitePress] project was created using GitLab Pages.

Learn more about GitLab Pages on their [product presentation page](https://about.gitlab.com/product/pages) and on [the official
documentation](https://docs.gitlab.com/ee/user/project/pages/).

## Participate
Ideally, it is preferable to edit this documentation locally: you will be able to see changes and problems early on.

If you do not know how to edit this documentation locally, you can edit sections of the code using GitLab's editor and create merge requests.

If you do not know either, it may be best to ask a colleague working at SDS for guidance.

## Writing locally

This project uses [yarn](https://yarnpkg.com) and node in version 18. Use [nvm](https://github.com/nvm-sh/nvm) to manage your node versions.

After installing the project with `$ yarn install`, you can spawn a local version of the project with `$ yarn run start`.

## Building locally
If you have trouble publishing, you may want to build the site locally.
You can do it with `$ yarn run build`, and serve this build locally with `$ yarn run preview`.

[VitePress]: https://vitepress.dev/

