# Projets

## Registre de projets
Nous tenons un [registre de projets](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::9d22a7e4-c968-4f91-8077-16400dbb5e04?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A13%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3Ab0d148e51ed04aa983751df72014aff2%40thread.skype%22%7D&tenantId=f7e124e1-3cbb-4714-b90e-d08652874b65) recensant les projets sur lesquels on intervient (ou sommes intervenus) en décrivant les éléments suivants :
+ Description
+ Client et contacts utiles
+ Partenaires + contacts
+ Référent SDS + équipe
+ Budget
+ Liens utiles


## Hub de nos applications
Nous disposons également d'un [hub de nos applications](https://apps.systra.digital/#/) déployées sur notre infrastructure (version démo, integ, staging, prod). 

![Hub de nos applications](../images/hub-des-applications.png)
<center><i> Hub de nos applications </i></center>
