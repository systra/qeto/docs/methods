export default {
    title: 'SDS Organisation et méthodes',
    description: "Documentation décrivant l'organisation et les méthodes de SDS",
    base: '/qeto/docs/methods/',
    outDir: '../public',
    themeConfig: {
        sidebar: [
          ['/', 'Accueil'],
          {
            text: 'Organisation transverse',
            collapsed: false,
            items: [
              {text:'Organisation', link: '/organisation-transverse/organisation.md'},
              {text:'Equipe de production', link:'/organisation-transverse/equipe-de-production.md'},
              {text:'Gestion des opérations', link:'/organisation-transverse/gestion-des-operations.md'},
              {text:'Maîtrise technique', link:'/organisation-transverse/maitrise-technique.md'},
              {text:'Management de proximité', link:'/organisation-transverse/management.md'},
              {text:'Progression continue et innovation', link:'/organisation-transverse/progression-continue-innovation.md'},
              {text:'Recrutement', link:'/organisation-transverse/recrutement.md'},
              {text:'Business développement', link:'/organisation-transverse/business-developpement.md'},
            ]
          },
          {
            text: 'Approche méthodologique',
            collapsed: false,
            items: [
              {text:'Approche UX Design', link:'/approche-methodologique/approche-ux-design.md'},
              {text:'Principe de la méthode Agile', link:'/approche-methodologique/principe-methode-agile.md'},
            ]
          },
          {
            text: 'Conception orientée utilisateurs',
            collapsed: false,
            items: [
              {text:'Identification des besoins', link:'/conception-orientee-utilisateurs/identification-des-besoins.md'},
              {text:'Recherche utilisateur', link:'/conception-orientee-utilisateurs/co-construction.md'},
              {text:'Spécifications des besoins', link:'/conception-orientee-utilisateurs/specification-des-besoins.md'},
              {text:'Charte UX - UX Sytem design', link:'/conception-orientee-utilisateurs/charte-UX-design.md'},
            ]
          },
          {
            text: 'Software Development Guide',
            collapsed: false,
            items: [
              {text:'Getting started', link:'/software-development-guide/README.md'},
              {text:'Jira Flow', link:'/software-development-guide/jira-flow'},
              {text:'Jira Custom Fields', link:'/software-development-guide/jira-custom-fields'},
              {text:'SDS Git flow and versioning', link:'/software-development-guide/sds-git-flow'},
              {text:'JavaScript style guide', link:'/software-development-guide/javascript-style-guide'},
              {text:'Python style guide', link:'/software-development-guide/python-style-guide'},
              {text:'Code reviews', link:'/software-development-guide/code-reviews'},
              {text:'Creating version tags with CI jobs', link:'/software-development-guide/tag-ci-task'},
              {text:'Keeping changelogs', link:'/software-development-guide/changelogs'},
            ]
          },
          {
            text: 'Développement itératif agile',
            collapsed: false,
            items: [
              {text:'Pratiques de développement', link:'/developpement-iteratif-agile/pratiques-de-developpement.md'},
              {text:'Pratiques de test', link:'/developpement-iteratif-agile/pratiques-de-tests.md'},
            ]
          },
          {
            text: 'Application finale',
            collapsed: false,
            items: [
              {text:'Hébergement et maintenance infrastructure', link:'/application-finale/hebergement-et-maintenance-infrastructure.md'},
              {text:'Support utilisateur et maintenance applicative', link:'/application-finale/support-utilisateur-maintenance-applicative.md'},
              {text:'Coordination, Pilotage, Reporting', link:'/application-finale/coordination-pilotage.md'},
              {text:"Evaluation et suivi de l'expérience utilisateur", link:'/application-finale/evaluation-suivi-UX.md'},
            ]
          },
          {
            text: 'Application Hosting',
            collapsed: false,
            items: [
              {text:'Hosting eligibility and responsibilities', link:'/application-hosting/eligibility-and-responsibilities.md'},
              {text:'Redémarrer le server SDS/scaleway', link:'/application-hosting/server-maintenance-reboot-procedure.md'},
              {text:'Code hosting and Continuous Integration', link:'/application-hosting/code-hosting-and-continuous-integration.md'},
              {text:'Deployment', link:'/application-hosting/deployment.md'},
              {text:'Automated hooks', link:'/application-hosting/automated-hooks.md'},
              {text:'Manual services', link:'/application-hosting/manual-services.md'},
              {text:'Rules for hosting source code on gitlab.com', link:'/application-hosting/gitlab-rules.md'},
            ]
          },
          {
            text: 'Outils et technologies',
            collapsed: false,
            items: [
              {text:'Outils et technologies', link:'/outils-et-technologies/outils-et-technologies.md'},
            ]
          },
          {
            text: 'Projets',
            collapsed: false,
            items: [
              {text:'Projets', link:'/projets/projets.md'},
            ]
          },
        ]
      }
}
