# Charte UX - UX System design

Pour la production de maquettes haute-fidélité nous nous appuyons sur un [design system d’expérience utilisateur](https://www.figma.com/file/kqdkZYf61cUfhBKroV22nq/Charte-UX?node-id=0%3A1) qui comprend les éléments suivants :
+ Éléments généraux : palette chromatique (déclinable selon les besoins d’un client particulier), typographies
+ Composants d’interface : boutons, icônes, cartes, pop-up, snackbars, formulaires, arborescences, tableaux, steppers, onglets…
+ Écrans types :  layout, cartographie, dashboard, vue 3D BIM

![Exemple de description d’un composant d’interface dans notre design-system](../images/design-system-composant-interface.png)
*Exemple de description d’un composant d’interface dans notre design-system*

D'autres ressources UI-UX sont disponibles [ici](https://systragroup.sharepoint.com/:f:/r/sites/SYSTRADigital/Documents%20partages/General/UI-UX%20Design?csf=1&web=1&e=7TQ7V1)

