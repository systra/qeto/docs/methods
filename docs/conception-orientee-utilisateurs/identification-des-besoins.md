# Identification des besoins métiers

Notre démarche de développement débute donc par une identification des besoins métiers. L’objectif est de recenser les besoins, de hiérarchiser les priorités, de comprendre les utilisateurs et leur contexte et de dessiner des parcours utilisateur cibles. Nous aimons beaucoup fonctionner avec des ateliers collaboratifs pour décrire des personae, dessiner des parcours utilisateur et hiérarchiser des besoins.

![Exemple de rendu d’atelier sur un process métier](../images/atelier-persona-storyboarding-usecase.png)
*Exemple de rendu d’atelier sur un proces métier*


![Exemple de rendu d’atelier persona / storyboarding / use case](../images/atelier-process-metier.png)
*Exemple de rendu d’atelier persona / storyboarding / use case*


![Exemple de rendu d’atelier de priorisation](../images/atelier-priorisation.png)
*Exemple de rendu d’atelier de priorisation*


- Inspiration / Partage de connaissance
- Analyse de l’existant
- Interviews / Visites sur site / Workshop
- Identification des problèmes et attentes
- Persona / Storyboarding / Use cases

Elle a pour but d’étudier les fonctionnements actuels, de relever les non-dits et d’assurer l’appropriation future de l’application. Lors de cette phase d’ethnographie, nous allons sur le terrain interviewer les futurs utilisateurs.

Nous nous appuyons sur les outils suivant :
-	Interviews / Visites sur site / Workshop
-	Observation
-	Identification des problèmes et des attentes
-	Collecte d’information
-	Co-construction
