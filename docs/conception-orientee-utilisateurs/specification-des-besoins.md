# Spécifications des besoins

La troisième étape de la conception consiste à spécifier une version de l’application. Nous construisons avec nos clients des parcours utilisateur traduits en fonctionnalités, des maquettes d’interface et d’interactions, un dossier d’architecture technique.
 Sur la base de l’ensemble de ces éléments nous cadrons le planning et le budget du développement.

![Exemple de maquettage d’interface haute et basse fidélité sur la base d’une sortie d’atelier](../images/maquettage-high-and-low-fidelity.png)
*Exemple de maquettage d’interface haute et basse fidélité sur la base d’une sortie d’atelier*

- Mockups intéractifs
- Choix des technologies 
- Choix de l’architecture technique
- Définition du backlog
- Coût & Planning (releases & sprints)
