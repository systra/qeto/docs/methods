# Recherche utilisateur

La seconde étape est la recherche utilisateur. Elle a pour but d’étudier les fonctionnements actuels, de relever les non-dits et d’assurer l’appropriation future de l’application. Lors de cette phase d’ethnographie, nous allons sur le terrain interviewer les futurs utilisateurs.

Nous nous appuyons sur les outils suivants :
-	Interviews / Visites sur site / Workshop
-	Observation
-	Identification des problèmes et des attentes
-	Collecte d’information
-	Co-construction

![Exemple de questionnaire utilisateur](../images/questionnaire-utilisateur.png)
*Exemple de questionnaire utilisateur*

