# Gestion des opérations

## Organisation des sprints 

Nous fonctionnons avec un calendrier de sprint qui s'applique à l'ensemble de nos projets. Nous nous appuyons sur des sprints de 3 semaines en général (ponctuellement de 2 ou 4 semaines). 

Le calendrier des sprints à venir est disponible sur le fichier de Roadmap que l'on trouve au [lien suivant](https://systragroup.sharepoint.com/:x:/r/sites/SYSTRADigital/Documents%20partages/General/Op%C3%A9rations/Roadmap/Roadmap.xlsx?d=w45ad8fdc814a40da9d9dac18c72bc01f&csf=1&web=1&e=jnswFB)

Et ci-dessous se trouve une planification typique d'un sprint avec les différents rituels qui le ponctuent. 

![Calendrier de sprint](../images/calendrier-sprint.png)
*Calendrier de sprint*

Nos différents rituels sont décrits ci-dessous:

|Rituels| Objectifs                                                                                                                                                                                                                                                                                  |Participants|Durée|
|:---|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:----|:----|
|**Préparation de sprint**| <ul><li>Arbitrer entre les projets pour le staffing du sprint à venir</li><li>Préparer en amont la réunion de lancement par sous-équipes projets pour plus d'efficacité</li><li>Identifier les besoins à court/moyen terme en ressources, et les besoins de montée en compétence</li></ul> |Sous-équipes projets|de 5' à 1h selon les projets|
|**Lancement de sprint**| <ul><li>Définir une prévision partagée par tous des fonctionnalités livrables en fin de sprint</li></ul>                                                                                                                                                                                       |Toute l'équipe|1 heure|
|**Daily Meeting**| <ul><li>Mettre au courant de l'avancement des tâches (mais ce n'est pas le lieu pour discuter dans le détail de l'avancement sur les tâches)</li><li>Faire remonter des alertes sur la production</li></ul>                                                                                |Toute l'équipe|15'|
|**Clôture de sprint - Revue de sprint**| <ul><li>Montrer ce qui a été réalisé afin d'en tirer des conséquences pour les suites des projets</li><li>Faire remonter des alertes sur la production</li></ul>                                                                                                                           |Toute l'équipe + éventuellement Product owners externes|30'|
|**Clôture de sprint - Retrospective de sprint**| <ul><li>Montrer ce qui a été réalisé afin d'en tirer des conséquences pour les suites des projets</li><li>Faire remonter des alertes sur la production</li></ul>                                                                                                                           |Toute l'équipe|45'|


[Comptes rendus des clôtures de sprint](https://systragroup.sharepoint.com/:f:/r/sites/SYSTRADigital/Documents%20partages/General/Scrum?csf=1&web=1&e=M8uxd4)


Nous nous appuyons sur l'outil [JIRA](../outils-et-technologies/outils-et-technologies.md) pour la gestion du backlog des différents projets, qui sont rassemblés dans le projet [SPRINTS](https://qeto.atlassian.net/secure/RapidBoard.jspa?rapidView=5&projectKey=SPRINTS)


## Gestion de la charge et du staffing

Le pilotage de la charge et du staffing se fait à 3 niveaux :
+ **à l'échelle du sprint** : lors des lancements de sprint, nous vérifions la compatibilité des charges affectées à une personne avec son temps disponible, et ainsi pouvons de manière collective définir un engagement qui soit réaliste par rapport aux disponibilités de l'équipe
+ **à 3 mois** : en s'appuyant sur le backlog des différents projets en cours déjà identifiés et des prospects avec une forte probabilité de gain intégrés à l'outil JIRA, pour faire des scénarios de staffing sur les projets et identifier le planning réaliste pour les délivrer. Pour cela nous nous appuyons sur le fichier [Roadmap](https://systragroup.sharepoint.com/:x:/r/sites/SYSTRADigital/Documents%20partages/General/Op%C3%A9rations/Roadmap/Roadmap.xlsx?d=w45ad8fdc814a40da9d9dac18c72bc01f&csf=1&web=1&e=jnswFB)
+ **à 6 mois / 1 an** : en s'appuyant sur un pipe commerciale intégrant les prospects et offres pour construire un [plan de charge prévisionnel](https://systragroup.sharepoint.com/:x:/r/sites/SYSTRADigital/Documents%20partages/General/Finance/Qeto_commercial%20pipe%20+%20pdc.xlsx?d=w451ef5e24398464e8e71b9b45485ecfa&csf=1&web=1&e=5xc30W
), et identifier les ressources nécessaires.


## Gestion de la sous-traitance

Nous nous appuyons ponctuellement sur de la sous-traitance.

XXX Faire référence à procédure de gestion des sous-traitants de SYSTRA XXX

## Evaluation de la satisfaction client

[Relation client](https://systragroup.sharepoint.com/sites/SYSTRADigital/_layouts/OneNote.aspx?id=%2Fsites%2FSYSTRADigital%2FSiteAssets%2FBloc-notes%20de%20SYSTRA%20Digital&wd=target%28Sales.one%7CE6D80ECE-74B1-4ABD-BB49-D05F2C6417B7%2FSatisfaction%20client%7C30C8D1BA-DCE4-4F7E-BBE8-367A9318E6C0%2F%29
onenote:https://systragroup.sharepoint.com/sites/SYSTRADigital/SiteAssets/Bloc-notes%20de%20SYSTRA%20Digital/Sales.one#Satisfaction%20client&section-id={E6D80ECE-74B1-4ABD-BB49-D05F2C6417B7}&page-id={30C8D1BA-DCE4-4F7E-BBE8-367A9318E6C0}&end)

Comment avez-vous connu SYSTRA ? 
Quelle image aviez-vous ? Quelle image avez-vous maintenant ? 
Quels éléments ont été déterminants dans le choix de SYSTRA ? 
Points forts
Axes d'amélioration

Lien avec enquêtes SMI SYSTRA ?

Enquête qualitative
+ Ecoute 
+ Réactivité
+ Conseil et accompagnement
+ Organisation du projet
+ Reporting et communication
+ Pilotage des autres acteurs
+ Gestion du planning et des délais
+ Qualité technique des livrables
+ Qualité de la présentation
+ Innovation et créativité
+ Comportement éthique


