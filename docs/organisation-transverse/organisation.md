# Organisation


## Organigramme

Notre organisation s'appuie sur 4 axes :
+ Axe ingénierie
+ Axe technique
+ Axe opérations
+ Axe business developpement

![Notre organigramme](../images/organigramme.png)

## Comité managerial
Notre comité managérial se compose du Chief digital officier, du responsable ingénierie, responsable technique, responsable opérations et du responsable business development.


## Rituels

|Rituels| Ordre du jour                                                                        |Participants|Fréquence|Durée|
|:---|:-------------------------------------------------------------------------------------|:----|:----|:----|
|Coordination commerciale/projet| <ul><li>Tour météo</li><li>Point à aborder en séance</li><li>Suivi actions</li></ul> |Comité managerial|Hebdomadaire|45'|
|Réunion d'équipe| <ul><li>Tour météo</li><li>Actualités groupe SYSTRA</li><li>Actualités SDS</li></ul> |Toute l'équipe|Tous les mois|1h30|
|Journée équipe| Ordre du jour selon actualité                                                        |Toute l'équipe|Tous les 6 mois|3h à 1 journée|


## Modes de fonctionnement

### Communication

[Equipe Teams SDS](https://teams.microsoft.com/l/team/19%3a75ec716336e7437ba559d690ac8ca6a0%40thread.skype/conversations?groupId=98cf44e8-b2df-4a23-9da3-8c49d64cec1a&tenantId=f7e124e1-3cbb-4714-b90e-d08652874b65)


**Message à toute l'équipe** : SYSTRADigital@systragroup.onmicrosoft.com
Astuce :
Contrôler les messages reçus du groupe Outlook : https://outlook.office.com/owa/?path=/group/SYSTRADigital@systragroup.onmicrosoft.com/mail
/ Paramètres du groupe / Gérer le courrier du groupe

**Message à toute l'entreprise**: Yammer https://www.yammer.com/systra.com/#/threads/inGroup?type=in_group&feedId=4704668

**Partages** : canaux Teams dédiés + OneNote associé

**Pour le reste** : classique email, Skype, MSN

### Outils de travail quotidien

La liste complète des outils et technologies que nous pratiquons est décrite [ici](../outils-et-technologies/outils-et-technologies.md). 

D'une manière générale, pour créer des comptes dans les outils que nous utilisons (Gitlab, Jira, Figma...) créer un compte avec l'adresse @systra.com pour ne pas mélanger les comptes pro et perso.


### Partage de connaissance

(**A VOIR SI ON LAISSE CELA OU BIEN ON LE MET AILLEURS)

**About us / wiki**: décrit les modes de fonctionnement de SYSTRA digital. Information assez statique utile pour les nouveaux arrivants.

 **Comptes-rendus de réunion** : 
+ Meeting notes - external [(Mode web)](https://systragroup.sharepoint.com/sites/SYSTRADigital/_layouts/OneNote.aspx?id=%2Fsites%2FSYSTRADigital%2FSiteAssets%2FBloc-notes%20de%20SYSTRA%20Digital&wd=target%28Meeting%20notes%20-%20external.one%7C79E59921-7170-4DF0-93FE-3F87A17F3BFD%2F%29)
+ Meeting notes - internal  [(Mode web)](https://systragroup.sharepoint.com/sites/SYSTRADigital/_layouts/OneNote.aspx?id=%2Fsites%2FSYSTRADigital%2FSiteAssets%2FBloc-notes%20de%20SYSTRA%20Digital&wd=target%28Meeting%20notes%20-%20internal.one%7C071C6D2D-E62E-41D3-829F-5DB647FCA3A2%2F%29)

<i>Bonnes pratiques :
nommer la réunion 20190425_Description de la réunion
créer une page OneNote depuis une réunion Outlook et la déplacer dans le OneNote partagé pour obtenir la liste des participants et les pièces jointes</i>

 
**Documents internes à l'équipe** : Sharepoint associé à Teams/SYSTRA Digital
https://systragroup.sharepoint.com/sites/SYSTRADigital/Documents%20partages
 
**Documents de projets à partager avec des externes à l'équipe** : à définir
 
**Documents d'aide au développement** : ?

### Autres Processus

Notre organisation s'appuie sur les processus suivants :
+ [Maîtrise technique](/organisation-transverse/maitrise-technique.md)
+ [Progression continue et innovation](/organisation-transverse/progression-continue-innovation.md)
+ [Gestion des opérations](/organisation-transverse/gestion-des-operations.md)
+ [Management de proximité](/organisation-transverse/management.md)
+ [Recrutement](/organisation-transverse/recrutement.md)
+ [Business développement](/organisation-transverse/business-developpement.md)

