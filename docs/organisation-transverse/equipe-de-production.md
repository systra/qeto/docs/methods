# Equipe de production


## Esprit d'équipe

Au sein de l'équipe, nous aimerions voir se développer les comportements suivants : 
+ entraide, écoute et dépassement de fonction
+ curiosité et envie d'apprendre
+ bienveillance, "assume positive intent"
+ ouverture, transparence et partage d'information
+ audace


## Rôles et responsabilités

+ **Scrum Master** : organiser et faciliter les rituels agiles

+ **Product owner** : définir le backlog, prioriser et écrire les histoires utilisateurs ou user stories // Conseil, conduite de projet et pilotage du développement
+ **« Proxy » product owner** 
+ **UX Designer** : Recherche utilisateur, design UX et UI, tests utilisateurs
+**Technical leader** : xxxx
+ **Ingénieur.e.s développement** : implémentent les fonctionnalités basées sur les user stories, // Technologies web, data engineering, DevOps



