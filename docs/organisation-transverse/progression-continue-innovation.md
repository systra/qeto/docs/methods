# Progression continue et innovation

Nous sommes guidés par l'envie d'apprendre et de nous améliorer. Pour cela nous avons mis en place les différents dispositifs suivants : 
+ Retrospectives de sprint
+ Partages techniques
+ Usine à idées
+ Veille technique
+ Hackathon

## Retrospectives
Nous nous appuyons dans le cadre de notre démarche scrum sur les rétrospectives de sprint pour améliorer nos processus.

https://qeto.atlassian.net/secure/RapidBoard.jspa?rapidView=10&projectKey=AC


## Partages techniques

Nous réalisons au sein de notre équipe des partages techniques réguliers (à chaque sprint) entre membres de l’équipe afin de favoriser la montée en compétence technique, l’ouverture à la multidisciplinarité ainsi qu’au développement d’une vision d’ensemble de notre processus de production.

Vous trouverez des informations concernant les partages techniques dans le [Guide pour les partages techniques](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::7425ef22-001a-4f7c-999f-d51c6ff28741?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A4%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3Ac2684740dfb4463ea26669cd7c43b196%40thread.skype%22%7D&tenantId=f7e124e1-3cbb-4714-b90e-d08652874b65).


## Veille technique

Nous réalisons continuellement une veille à la fois d’un point de vue méthodologique et d’un point de vue technologique sur les logiciels et composants que nous utilisons, mais également sur les nouvelles technologies émergentes.

 ## Usine à idées

"L'usine à idées" est le canal sur lequel chacun peut soumettre des idées dans tous les domaines du fonctionnement de SDS (technique, organisation, commercial…) et auquel chacun peut participer en complétant, précisant et discutant ces idées. Si certaines ont déjà pu être avancées via le canal de discussion, tout le monde n'a pas encore eu la possibilité de participer ou de consulter les idées dans le détail, ce qui peut se comprendre car nous n'avons pas toujours du temps à y consacrer.

[Usine à idées - Readme](https://teams.microsoft.com/l/entity/com.microsoft.teamspace.tab.wiki/tab::40d9d0db-12be-495d-9fb8-a6c24517c6d3?context=%7B%22subEntityId%22%3A%22%7B%5C%22pageId%5C%22%3A2%2C%5C%22origin%5C%22%3A2%7D%22%2C%22channelId%22%3A%2219%3A8cfee00c925f43118505b765ae90e0ca%40thread.skype%22%7D&tenantId=f7e124e1-3cbb-4714-b90e-d08652874b65) 


[Template vide Usine à idées](https://systragroup.sharepoint.com/:w:/r/sites/SYSTRADigital/Documents%20partages/Usine%20%C3%A0%20id%C3%A9es/[aide]%20Template%20Vide.docx?d=w6ef054f1f4e1411cb431cfa250b20373&csf=1&web=1&e=VGoRya)


[Exemple d'idée](https://systragroup.sharepoint.com/:w:/r/sites/SYSTRADigital/Documents%20partages/Usine%20%C3%A0%20id%C3%A9es/[aide]%20Exemple%20id%C3%A9e%20-%20Calendrier.docx?d=wa3f0a46614ee4dbbb2de692eb8f0e715&csf=1&web=1&e=nQeaBa)

## Hackathon
Objectifs 
1. S'amuser et passer un bon moment ensemble
2. Découvrir d'autres métiers
3. Produire des preuves de concept : parcours utilisateur, maquettes, notebooks, schéma d'architecture, no code, code, powerpoint, estimations de développement …
Sous le format usine à idée, un document par idée, support du pitch d'introduction


