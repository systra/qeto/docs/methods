# Recrutement

### Principes

"Recruit with the fear to miss talents, not with the fear to recruit somebody who doesn't fit"


### Processus

Notre processus de recrutement s'appuie sur les éléments suivants :
+ Premier entretien dont l'objectif prioritaire est de s'assurer que le candidat est en adéquation avec notre culture
+ Deuxième entretien (couplé avec un troisième le cas échéant) dont l'objectif est de tester le candidat sur un cas d'étude concret, et d'en débriefer avec lui
+ Dernier entretien : RH

### Principe du challenge 

Objectifs du challenge : 

Le challenge est une mise en situation volontairement vaste, aux contours de mission mal définis, avec un manque d'information, peu d'aide disponible et peu de temps. 
Nous cherchons à tester ta capacité à construire une méthodologie face à un problème nouveau, dans un temps court et ta capacité à produire des premiers résultats. 
Les critères d'évaluation sont : 

	- La pertinence. Compréhension des enjeux et prise en compte des contraintes. Concrètement, nous ne sommes pas Google, on ne pourra pas staffer 50 PhD sur un projet de 50 M€.
	- Le pragmatisme. Plutôt que de formuler des recommandations, et de donner des conseils, on préfère un plan d'action concret avec des étapes intermédiaires. On va accorder de la valeur à un challenge qu'on pourrait réellement mettre en œuvre. 
	- L'audace. Critère bonus : viser haut tout en étant pragmatique. 

Livrable :
Nous sommes ouverts sur le fond comme sur la forme : vidéo, site web, powerpoint (max 10/12 slides), infographie, chanson, planche de skate dessinée. 
La seule contrainte est de produire tous les livrables en anglais. 

Délai et durée : 
7 jours à partir d'aujourd'hui. En étant raisonnable, il faudrait passer entre 4 et 6h maximum sur le challenge, ça reste un exercice. 
Nous savons que tu travailles et que tu as une vie en parallèle, si vraiment tu as des contraintes dans les 7 jours qui viennent, dis-nous. 

Problématique à traiter :
+ Product management
+ Conseil en transformation data
+ Data analyst
+ UX design

 
Remarque concernant la production : 
Nous nous engageons à ne pas utiliser ce que tu pourras produire dans le cadre de ce challenge dans le cadre de nos activités commerciales. L'exercice restera strictement un exercice du processus de recrutement. 


https://systragroup.sharepoint.com/:f:/r/sites/SYSTRADigital/Documents%20partages/General/Recrutement/Questions?csf=1&e=YzWF2b


https://systragroup.sharepoint.com/:f:/r/sites/SYSTRADigital/Documents%20partages/General/Recrutement/Challenges%20tests%20de%20code?csf=1&e=FpCs72




### Intégration nouveau développeur

Demande de matériel pour développeur

Les instructions générales pour les demandes de matériel spécifiques aux développeurs sont sur le [Guide de démarrage des dév DCO](https://systragroup.sharepoint.com/sites/cop-softwaredev/SitePages/SYSTRA%20Software%20Dev%20Getting%20Started_fr.aspx).
 
Sinon, voici les textes spécifiques à mettre dans les demandes :
 
Message à mettre dans la demande de PC : (formulaire sur my.systra)
 
Bonjour,
Je me permets de demander un renouvellement de PC car, en tant que développeur, j'ai besoin d'un ordinateur de calcul performant, ce qui n'est pas actuellement le cas. Je cite par exemple d'ordinateur de calcul le modèle Dell Precision 3510, avec 16Go de RAM et un HDD de 500Go. Cette demande a été validée par ma hiérarchie. Je vous remercie par ailleurs de contacter JF Diard ou Richard Vivien à ce sujet si vous avez besoin de validation/confirmation.
Merci à vous.
 
Message à mettre dans la demande de compte admin sur PC : (une fois que l'on a reçu le nouveau PC, à envoyer à support@systra.com, avec JF Diard et Paula en copie)
 
Bonjour, 
En tant que développeur informatique, je me permets de vous écrire afin de demander un compte admin sur mon PC, {insérer nom du PC ici, type LWPFRFAR…}. J'ai besoin de ce compte afin de créer mon environnement de travail et lancer mes outils. Je vous remercie de contacter JF Diard si vous avez besoin de valider cette demande.


