# Outils et technologies

Nous maîtrisons des technologies dans les domaines suivants : outil de design, gestion de projet agile, gestion de versions et d’intégration continue, de développement front-end et back-end, de gestion de base de données, de conteneurisation logicielle, ainsi que d’autres services nécessaires à la réalisation de nos applications. 

Ci-dessous les principales technologies que nous utilisons :

![Liste technologies](../images/technos.png)

## Gestion de projet

### Gestion du backlog et du sprint : Jira

Jira est un outil de gestion de projet agile. Pour chaque projet, nous y définissons les « user stories » et les tâches de design et développement. Nous l’utilisons également pour piloter nos sprints : définition du backlog, estimation de la complexité, allocation des tâches, suivi qualité et des revues de code, suivi du temps. 

![JIRA](../images/jira.png)

### Gestion de versions et d’intégration continue : GitlaB
Nous utilisons Gitlab ([https://gitlab.com/systra](https://gitlab.com/systra)) comme un outil de gestion des versions et d’intégration continue sur l’ensemble de nos projets, en appliquant un certain nombre de bonnes pratiques pour la gestion des branches et des commits.



### Documentation projet en ligne
Lorsque nous produisons de la documentation projet en ligne, nous nous appuyons sur Vuepress et le server de pages web statiques de Gitlab.

Vous pouvez trouver un exemple de documentation projet [ici](https://systra.gitlab.io/visator/vizeo-doc/)


## Design
### Outil de maquettage : Figma
Figma est un outil de design. Nous l’utilisons pour la préparation et la restitution des ateliers de conception, ainsi que pour le maquettage des interfaces de nos solutions.

![Vue de l'outil Figma](../images/figma.png)

### Logiciel d'idéation et de collaboration : Miro
Miro est une plateforme d'idéation et de création de projet collaborative.

### Ressources graphiques d'icônes : Font Awesome
Nous nous appuyons sur les icônes issues du site [Font Awesome](https://fontawesome.com/icons?d=gallery). Les icônes que l'on utilise sont disponibles sous forme de fichiers svg [ici](https://systragroup.sharepoint.com/:f:/r/sites/SYSTRADigital/Documents%20partages/Communication/Autres/FontAwesome?csf=1&web=1&e=dOhW6z)


## Développement

### Développement Front-End : Vue.JS / React.JS / Backbone.JS

Côté front-end nous développons majoritairement sur le framework open source Vue.Js, ainsi que sur le framework React.JS. Ces deux frameworks font partie des trois frameworks les plus utilisés et les populaires de ces dernières années (avec Angular.js). Dans le cadre de certains projets, nous avons également utilisé la technologie Backbone.js que nous maîtrisons.


### Développement Back-End : Python + Django / Node.JS

Côté back-end, nous développons majoritairement sur Django qui est un framework Python de haut niveau permettant un développement rapide intégrant des exigences fortes de sécurité et maintenabilité. Nous utilisons également la technologie Node.js notamment sur des projets comportant des intégrations avec la solution Autodesk Forge

### Système de gestion de bases de données : MongoDB, PostgreSQL / POSTGIS

Pour les systèmes de gestion de base de données nous nous appuyons généralement sur :
+ PostegreSQL pour nos systèmes de gestion de base de données (SGBD) relationnelles et objet, s’agissant du système opensource le plus avancé,
+ de son extension PostGIS pour la manipulation d’informations géographiques et pour ainsi disposer d’un SGBD spatial ;
+ et de la plateforme MongoDB pour les systèmes de gestion de bases de données NoSQL (orientés documents), elle est opensource, et est la plus connue des bases de données NoSQL.

### Conteneurs logiciels : Docker

Pour la conteneurisation de nos applications nous nous appuyons sur le logiciel libre Docker qui est une plateforme de conteneurs ainsi que sur l’outil d’orchestration Docker Compose.

## Autres services

Selon les besoins des projets, nous nous intégrons à des services tiers. En particulier, nous réalisons des intégrations régulièrement  :
+ sur les sujets SIG : en utilisant les fonds de plan d’Open Street Map et les services de Mapbox,
+ et sur les sujet de Building Information Management avec Forge Autodesk.

![Mapbox et Forge](../images/mapbox-forge.png)
