
# Code hosting and continuous integration
[[toc]]
 
## Code source Hosting 

We very strongly advise you to use a versioning tool for your source code, and we also strongly advise you to choose git for that tool. The SDS team
needs to have access to the code source of any product that is deployed on our server: we will need to establish a
mirror to your repository on gitlab.com. 

Please consult the [Rules for source code hosting](./gitlab-rules.md) before creating your project on gitlab.com.

## Continuous integration (CI) 

Continuous integration is the act of establishing automated actions that are regularly performed against a code
base. In our context, it can mean running a linter for each new commit in the shared code repository to continuously
check that the code complies with a certain norm, or a secrets check that alerts the code owners if a password has been
erroneously put in the code. 

The SDS team manages a CI server able to execute such tests and a repository of continuous integration templates that help
set up linters, unit tests, type checkers, secrets checkers, CVE checkers, etc. 

Every app hosted on our server must have at least a CI enabled that checks [CVEs](https://cve.mitre.org/cve/). 

### Continuous integration setup
We use gitlab for our CI, which is configured in the `.gitlab-ci.yml` file at the root of the project. See, for example, the [.gitlab-ci.yml for tha backend of the application Vizeo](https://gitlab.com/systra/visator/visator-back/-/blob/master/.gitlab-ci.yml).

Please refer to [gitlab-ci-templates](https://gitlab.com/systra/qeto/infra/gitlab-ci-templates/-/blob/master/README.md) for instructions on setting up some of our standard CI tasks.

You can also refer to the [documentation on the CI task to create tags (versions)](../software-development-guide/tag-ci-task.md). 
