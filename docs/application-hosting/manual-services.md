## Manual services 

### CVE performance report [currently on hold, will resume for november/december 2022]

[CVE](https://cve.mitre.org/cve/) is a list of publicly disclosed cybersecurity vulnerabilities.

A report on CVEs in applications deployed on the SDS server is made each month. This report is distributed to the teams with an app hosted on the server and to the IT Security
representatives of SYSTRA.

This report contains, for each group of app, environment and package:
- number of CVE encountered per month for the last 6 months
- number of CVE cleaned per month for the last 6 months
- number of CVE remaining at the end of the month per month for the last 6 months
- average time to fix them (2 days accuracy) per month for the last 6 months