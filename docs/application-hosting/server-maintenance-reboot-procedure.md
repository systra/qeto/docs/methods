# Redémarrer le server SDS/Scaleway
Notre server héberge un ensemble d'applications, dont certaines sont en production et servent des centaines d'utilisateurs.
Nos conditions d'hébergement font que les projets hébergés acceptent des interruptions de service.

Il est parfois nécessaire de le redémarrer, par exemple pour appliquer des mises à jour de sécurité au niveau de l'OS.
[[TOC]]

# Protocole de communication
Ce protocole est pensé pour le server SDS/Scaleway.

## Date du redémarrage
Les redémarrages sont privilégiés en matinée et en début de semaine.

## Plan de communication
Il est prévu :
- un message au moins deux semaines avant l'opération
- un message de rappel une semaine avant l'opération
- un message de confirmation au début de l'opération
- un message toutes les 4 heures 
- un message de confirmation à la fin de l'opération
La communication doit être faite au moins une semaine avant l'opération, de manière à laisser le temps aux product
owners de diffuser l'information.

### Modèle 2 semaines avant
Bonjour,
Une opération de maintenance est prévue le YYYY-MM-DD au matin heure de Paris sur le serveur qui héberge vos services.

Aucune modification ne sera réalisée sur vos services.
Cette opération entraînera néanmoins une indisponibilité temporaire de vos services.
Cette indisponibilité devrait être courte (de l'ordre de la demi-heure). Il se peut néanmoins qu'elle soit plus longue : les dernières opérations de ce type que nous avons réalisées ont pu prendre jusqu'à trois heures.

Nous vous prions de préparer ce temps avec vos utilisateurs.
N'hésitez pas à nous prévenir d'une incompatibilité importante dans l'emploi du temps de votre application.

Cette opération de maintenance est nécessaire au maintien en conditions opérationnelles du serveur et à sa sécurité. Elle est réalisée généralement à un rythme trimestriel.

Nous vous rappellerons cet évènement dans une semaine, puis à la date d'intervention. Une fois l'intervention effectuée, nous vous tiendrons informés.

Cordialement,
L'équipe SDS

### Modèle une semaine avant
Bonjour,

Comme évoqué la semaine dernière, une opération de maintenance est prévue le YYYY-MM-DD au matin heure de Paris sur le serveur qui héberge vos services.

Aucune modification ne sera réalisée sur vos services.
Cette opération entraînera néanmoins une indisponibilité temporaire de vos services.
Cette indisponibilité devrait être courte (de l'ordre de la demi-heure). Il se peut néanmoins qu'elle soit plus longue : les dernières opérations de ce type que nous avons réalisées ont pu prendre jusqu'à trois heures.

Nous vous prions de préparer ce temps avec vos utilisateurs.

Nous vous rappellerons cet évènement à la date d'intervention. Une fois l'intervention effectuée, nous vous tiendrons informés.

Cordialement,
L'équipe SDS


### Modèle le jour J au début de l'intervention
Bonjour,

Comme évoqué la semaine dernière et la semaine passée, une intervention de maintenance est prévue ce jour.
Nous commençons l'opération.

Aucune modification ne sera réalisée sur vos services.
Cette opération entraînera néanmoins une indisponibilité temporaire de vos services.
Cette indisponibilité devrait être courte (de l'ordre de la demi-heure). Il se peut néanmoins qu'elle soit plus longue : les dernières opérations de ce type que nous avons réalisées ont pu prendre jusqu'à trois heures.

À l'issue de cette intervention ou en cas d'impondérable, nous vous tiendrons informés.

Cordialement,
L'équipe SDS

### Modèle le jour J en cas d'impondérable
Bonjour,

L'intervention de maintenance sur le serveur hébergeant vos services ne se passe malheureusement pas comme anticipé.

[expliquer les problèmes]

Si vous avez paramétré vos services pour activer les sauvegardes, les données de vos services ont bien été sauvegardées.
[selon gravité, précisé si on a perdu les données de la période ou pas]

[expliquer le temps estimé de rétablissement]


Cordialement,
L'équipe SDS


### Modèle le jour J après l'intervention

Bonjour,

L'intervention prévue ce jour sur le serveur hébergeant vos services est maintenant terminée.

Vos services sont à nouveau disponibles, et n'ont subi aucune modification.
Si toutefois vous constatiez une indisponibilité, n'hésitez pas à revenir vers nous au plus tôt.

Vous pouvez informer vos utilisateurs du rétablissement de la disponibilité de vos services.

Cordialement,
L'équipe SDS

## Liste des destinataires
La liste des destinataires est maintenue par l'équipe SDS et centralisée dans un document placé dans la section
technique du sharepoint. Il convient de la contrôler et de l'amender si besoin avant envoi.

# Protocole technique
 
**Première tentative : sans disable docker**
- arrêter le service docker `systemctl stop docker`
- faire toutes les mises à jour (aptitude)
- connexion à KVM over IP (sur le site d'online, dans la console de gestion du server)
- `systemctl kexec` ou `systemctl reboot`

**Seconde tentative (si la première entraîne un kernel panic) : avec disable docker**
- arrêter le service docker `systemctl stop docker`
- désactiver le service docker `systemctl disable docker`
- faire toutes les mises à jour (aptitude)
- connexion à KVM over IP (sur le site d'online, dans la console de gestion du server)
- `systemctl kexec` ou `systemctl reboot`
- redémarrer le service de docker `systemctl enable --now docker`
- démarrer les applications une à une


