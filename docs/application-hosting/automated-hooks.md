## Automated hooks 

The SDS team provides a series of default services for apps that are hosted on its server. 

### Backup 

Docker volumes backup can be kept and saved for a given number of days. This allows a backup of the application
data/database. This has to be parameterized on a product basis and is maintained by the code owners. The parameters
used can be seen on [this doc](https://gitlab.com/systra/qeto/infra/capp-hooks#backup-format).

### Monitoring 

For each app deployed in an environment on the SDS server, a monitoring dashboard is automatically created that follows
containers states, IO, RAM and CPU consumption, and can display health checks results if they have been added to the
product.
![Screen capture of a monitoring dashboard displaying graphs and numbers](../images/monitoring.png)
*Example of a monitoring dashboard*

### CVE analysis 

CVE, short for Common Vulnerabilities and Exposures, is a list of publicly disclosed computer security flaws. When
someone refers to a CVE, they mean a security flaw that's been assigned a CVE ID number. 

SDS runs a daily analysis searching for resolvable CVEs on every container on the server. A list is provided in a
ticketing tool (using Gitlab issues), allowing everyone to see the waiting CVEs, their locations and actions to solve
them, and mark them as fixed when they are. 

Here is a capture of the dashboard:  
![Screen capture of a kanban table with issues moving from todo to done](../images/cve-dashboard.png)
*Kanban board of CVE issues to address*

Here is a screen capture of a single issue, listing the CVEs and the packages to migrate for a specific app and
environment.
![Screen capture of a card displaying explicative elements on the issue and what to do](../images/cve-ticket.png)
*CVE ticket example, explaining the packages to update and the minimum version to update to*
