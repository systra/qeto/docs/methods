# Deployment

## Automated deployment 

SDS can help you put in place an automated deployment, that deploys your app on our server when you change a file,
asking for this deployment. This tremendously helps keeping track of what has been deployed where and when. 

## Deployment format 

All product deployed on the SDS server shall be containerized using Docker and its containers shall be orchestrated
using docker-compose, and the configuration shall be packaged in a specific format (DCA format, see DCA example). 
