# Rules for hosting source code on gitlab.com

[[toc]]

The SDS team administers a space for code source hosting and versioning at https://gitlab.com/systra/. If you wish to use this service, you must respect the following guidelines.

## Only create projects in the sub-group of your team or department

If a sub-group for your team does not exist, contact SDS in order to create one. Do not create your SYSTRA projects in your personal space on gitlab.com.

## Projects must be private by default

Your team's sub-group will be configured so that all projects in it are private. If you want to make a project public (i.e. readable by the whole world), please contact SDS to discuss your case.

## Secrets must not be stored in plain text on gitlab

Secrets include passwords, secret codes, API keys... 

Secrets can NEVER be pushed to gitlab, not even with the idea that they will be removed later. If you push a commit with a secret to gitlab.com, then push another commit removing it, the secret still exists in the history in gitlab.

### Encrypted secrets

It is possible, but not required, to store encryped secrets on gitlab. Please refer to [config-secret](https://gitlab.com/systra/qeto/infra/config-secret) for information on how to do this for your repository.

## You must enable the secret-detection CI in your project

CI is [continuous integration](./code-hosting-and-continuous-integration.md#continuous-integration-ci), automated actions that are performed regularly against a code base.

The secret-detection CI will detect if a secret has been pushed to a repository and generate an alert associated with the impacted branch in gitlab.

To enable the secret-detection CI, in the root of your project, create a file named `.gitlab-ci.yml` with the following contents:

```
include:
  - remote: `'https://gitlab.com/systra/qeto/infra/gitlab-ci-templates/raw/master/secret-ci.yml'`
secret: 
  extends: .secret
```
