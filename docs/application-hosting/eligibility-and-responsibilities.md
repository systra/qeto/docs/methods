# Hosting eligibility and responsibilities

The Software and Datascience Studio (SDS) team manages a server on which some products can be hosted.

We are a small team whose main area of focus is creating software. There is a limit to what we can do, how many apps we
can host and the means we can put in place if things go south.

[[toc]]

## Hosting eligibility 

The eligibility for hosting a product is decided on a per-product basis, and depends on the product's maturity, on its
level of innovation and on the ability for the team owning the product to maintain it. The choice is made in
collaboration with the innovation committee and the IT department. 

The first deployments shall be done jointly by the team that owns the code and SDS, so SDS can help set up all the
parameters and validate the compatibility between the app and the server capabilities. 

## Responsibilities for hosted apps 

Each team deploying an app on SDS’s server is responsible for its app, meaning how it is coded, maintained, secured. 

SDS provides a server and a deployment environment, with the comprehension that: 

- Server resources are not extensible: we have some room, but everyone is sharing the resources of a single server
instance. Your app shall be frugal. 
- The server may fall. We need up to two business days to make everything run again. This means that if the server
falls on Friday evening, it may not be running until Tuesday night (if there is no public holiday in between on the
French calendar). 

If an app is abusing the server resources or not updating its security, we reserve the right to stop or undeploy it,
after informing its team of these problems. The SDS team will try to offer a way to mitigate the problems but may need
to cut this app in order to protect all the others. 