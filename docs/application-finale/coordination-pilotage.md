# Coordination, Pilotage, Reporting

Nous réalisons des réunions d’exploitations dans nos projets de manière régulière afin de faire:
+ le suivi de l’exploitation de l’application sur la base d’indicateurs (d’utilisation, d’incidents…), 
+ l’évaluation de son fonctionnement, 
+ la décision sur des adaptations nécessaires à réaliser, 
+ l’étude et le suivi des demandes d’évolution, 
+ ainsi que le passage en revue des remontées utilisateurs et de la correction des incidents.
