# Support utilisateur et maintenance applicative

## Documentation utilisateur et formation

Pour la documentation utilisateur, nous nous basons sur le document de conception qui intègre l’expression fonctionnelle
des besoins et les visuels des interfaces associées, ainsi que sur des tutoriels utilisateurs en vidéo.
Selon les projets, nous pouvons mettre en place des documentations en ligne en utilisant Gitlab pages.

Nous réalisons par ailleurs des formations de prise en main de nos outils, ainsi que des FAQ (Foire aux Questions)
disponibles via l’application.

![Exemple de document de conception et de FAQ pour le support utilisateur](../images/support-utilisateur.png)
*Exemple de document de conception et de FAQ pour le support utilisateur.*

## Gestion des remontées utilisateurs

Chaque application dispose d’une adresse mail de support à laquelle les utilisateurs peuvent faire remonter des 
demandes : demandes d’informations, questions sur l’utilisation des produits, remontées de bug, ou suggestions de
fonctionnalités. 

Selon les projets nous nous appuyons sur un template de mail type pour que l’utilisateur puisse bien spécifier sa
demande.
![Exemple de mail type pour le contact support utilisateur](../images/contact-utilisateur.png)
*Exemple de mail type pour le contact support utilisateur.*

Le processus de traitement de ces demandes est présenté sur le schéma ci-dessous, et il est supporté par des outils
permettant d’en tracer le suivi.

![Processus de traitement des incidents et demandes d'évolutions](../images/traitement-des-incidents.png)
*Processus de traitement des incidents et demandes d'évolutions.*

Pour le traitement des demandes liées au support, nous nous appuyons sur l’échelle suivante : 
+ **Niveau 1. Simple, basique** : « Je n’ai pas accès à l’application » A ce niveau on vérifie l’environnement de 
travail, est-ce que le bon navigateur est utilisé, est-ce que l’utilisateur a bien un compte ? 
+ **Niveau 2. Fonctionnel** : C’est le support à l’utilisation. “Je ne sais pas comment effectuer telle action ?”
J’ai un problème avec cette fonction … Dans ce cas là, le product owner va réaliser ce support. 
+ **Niveau 3. Technique** : C’est un support qui va nécessiter l’intervention de nos développeurs pour résoudre un
problème technique.
+ **Demande d’évolution** : le product owner ira creuser l’idée d’évolution avec le demandeur (utilisateurs, gains
attendus, éventuel budget à allouer)

Le traitement de ces demandes est tracé et suivi, et s’il donne lieu à des développements intégré à notre outil de
gestion des développements. 

Les délais pour l’implémentation d’une correction sont définis par les SLA (Service Level Agreement) définis au niveau
de chaque projet et pourront être fonction du caractère d’urgence du problème
+ **Incident mineur** : pas de remise en cause de l’accès et du bon fonctionnement des fonctions et services ;
+ **Incident majeur** : des fonctions ou services ne sont pas accessibles, mais des solutions de contournement existent ;
+ **Incident bloquant** : des fonctions ou services inopérants.
