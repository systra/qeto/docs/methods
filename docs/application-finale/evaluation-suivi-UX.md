# Evaluation et suivi de l'expérience utilisateur


## Démarche et objectif

Au lancement de l'application, nous mettons en place une démarche de récolte des retours des utilisateurs qui s'articule de la manière suivante :
- Recueil des **attentes des utilisateurs** avant de découvrir l'application et **des premières impressions à chaud** quand les utilisateurs découvrent l'application 
- Retours des utilisateurs **après quelques semaines d'utilisation**
- Mise en place d'un **suivi périodique** (périodicité qui s'adapte aux enjeux des projets, en moyenne 6 mois)
- Mise en place de **recueils spécifiques sur des sujets d'évolution des besoins / métier**

L'objectif de cette démarche est de pouvoir mesurer l'**appréciation des utilisateurs** :
+ est-ce que mes utilisateurs aiment mon produit ? 
+ seraient ils prêts à l'utiliser ou en faire la promotion ?

et/ou de son **utilisabilité** :
+ est-ce que mon produit est utile, facile à utiliser et efficace ?

## Modalités de recueil 

Les modalités de recueil s'adaptent au contexte du projet et de ces utilisateurs dans un souci d'obtenir un taux de réponses satisfaisant en termes de volume et de qualité des réponses. 

Nous nous appuyons notamment sur :
+ Des questionnaires longs, moyens ou courts (avec des éléments qualitatifs et quantitatifs au travers notamment de questionnaires standardisés)
+ Des focus groupes, 
+ Des entretiens,
+ Des tests utilisateurs... 

## Suivi des évaluations UX

Nous nous appuyons sur un outil pour suivre les évaluations UX que nous menons 

![Suivi de l'utilisabilité](../images/suivi-utilisabilite.png)
*Outil de suivi de l'utilisabilité*

### Pourquoi ? 
+ Evaluer régulièrement les produits en interne
+ Capitaliser sur les données récoltées au fur et à mesure
+ Faciliter la réalisation des évaluations et l'accès aux résultats
+ Rendre autonomes les product owners


### Pour qui ? 
+ UX designers : tous les outils de calculs centralisés
+ Product owners : autonomes sur la consultation des résultats et le suivi
+ Commerciaux : arguments pour valoriser nos outils et nos méthodes


### Avec quelles échelles UX ?

Afin de mesurer l'évolution de l'utilisabilité de nos applications dans le temps, nous nous appuyons sur des questionnaires standardisés que nous choisissons selon le contexte de notre projet et de ces utilisateurs, et qui permettent de calcul des indicateurs d'expérience utilisateur. 

Nous nous basons principalement sur les indicateurs ci-dessous :


| Type  | Durée          | Utilisabilité ou Appréciation | Avec ou Sans détail |
| :--------------- |:---------------:| :-----:|:-----:|
|SUS  |   moyen (10 items)        |  utilisabilité | sans détail|
|UMUX  |   court (4 items)        |  utilisabilité | sans détail|
|DEEP  |   long (19 items)        |  utilisabilité | avec détails|
|CSUQ  |   long (16 items)        |  les deux | avec détails|
|attrack diff shorten  |   moyen (10 items)        |  les deux | avec détails|
|NPS  |   court (1 item)        |  appréciation | sans détail|
