# Hébergement et maintenance infrastructure

## Description de notre infrastructure 

Nous disposons de notre propre infrastructure pour le développement, l’hébergement et la maintenance de nos applications containérisées.

Voici le lien vers la documentation de notre infrastructure [https://systra.gitlab.io/qeto/docs/infra/](https://systra.gitlab.io/qeto/docs/infra/)
