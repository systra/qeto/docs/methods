# Pratiques de développement

[[toc]]

## Développement itératif

Pour le développement de nos applications, nous privilégions un développement itératif organisé en sprints de développement. Adopter ce mode de fonctionnement, c’est se donner le droit de réordonner les priorités. Le projet évolue à mesure que l’on découvre comment les premières fonctionnalités de l’outils sont utilisée.

## Devops

Dans l’esprit du cadre méthodologie agile, nous développons en mode devops ce qui nous permet d'automatiser et de monitorer l'ensemble du cycle de vie des applications, des phases de développement, test jusqu'à la mise en production.

## Environnements de développement

Nous nous appuyons sur les différents environnements suivants pour le développement de nos applications :
+ Environnement d’intégration : utilisés par les développeurs de l’équipe SDS,
+ Environnement de recette utilisé par les product owner et « proxy » product owner pour valider la mise en recette avant mise en production,
+ Environnement de production pour les utilisateurs finaux.

## Template de déploiement

Pour le déploiement de nos applications, nous nous basons sur un template de déploiement DCA (réalisé en python avec du undockerise) basé sur l’infrastructure que nous avons mis en place, et qui est présentée plus bas.

## Template applicatif front end

Lors de nos développements, nous nous appuyons sur un template applicatif front-end (ou boilerplate frontend) nous permettant de capitaliser sur les vues génériques développées dans l’ensemble de nos projets. Ce template applicatif est cohérent avec notre système de design d’expérience utilisateur et est supporté par deux des technologies front que nous utilisons : Vue.js et React.js. Il permet de gagner en efficacité lors du développement tout en assurant une meilleure maintenabilité de nos applicatifs.

![Template front](../images/template-front.png)
*Exemples de vues types de notre template applicatif (login, dashboard modulaire, liste de projets, arborescence d’actifs)*


## Template applicatif back end

Nous nous appuyons côté back-end au maximum sur des templates applicatifs (en particulier sur le template django rest framework) et des librairies permettant de capitaliser des services (par exemple : librairie d’authentification, librairies de traitement de GTFS,..).

## Authentification

Pour l’authentification, nous fonctionnons généralement avec une authentification qui utilise l'AD Azure de SYSTRA, en s'appuyant sur la bibliothèque [django-oauth2-authcodeflow](https://gitlab.com/systra/qeto/lib/django-oauth2-authcodeflow).

## Gestion des droits sur les applications
TO DO 

## Opensource

Nous privilégions lorsque possible l’utilisation de logiciels ou de composants de type opensource, dans l’objectif de garantir l’évolutivité et la pérennité des développements que nous réalisons.

Par ailleurs, lorsque nous publions du code source sous licence libre, nous le mettons à disposition sur Github : https://github.com/systragroup

## Bonnes pratiques de code
TO DO
Nous appliquons les bonnes pratiques de code pour assurer la maintenabilité de nos applications :
+ mise en œuvre** : architecture flexible et simple, conception modulaire,
+ auto-description du code, gestion des modes dégradés,
+ code testable facilement, rédaction de procédures de maintenance et d’exploitation…

**Faudrait faire référence plutôt à des guides de bonnes pratiques qu'on aurait écrites par ailleurs**


## Revue de code
Merci de vous référer à la page [Code reviews](../software-development-guide/code-reviews.md) pour les pratiques de revue de code. Vous pouvez également consulter la [checklist d'exemple pour s'assurer que son code est prêt à être relu](../software-development-guide/pre-code-review-checklist.md).

## Gestion des versions et d’intégration continue

Nous utilisons le [semantic versioning](https://semver.org/) pour nos projets. Normalement, nous utilisons la tâche 'tag' de l'intégration continue pour créer les tags de versions. 

Pour plus d'informations, référez-vous à [Creating version tags with CI job](../software-development-guide/tag-ci-task.md).
