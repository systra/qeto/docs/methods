# Pratiques de test

## Stratégie de test

Nous définissons une stratégie de test adaptée à chaque projet selon les enjeux de nos clients, les types d’utilisateurs et le niveau de complexité de l’application. Cette stratégie s’appuie sur différents types de tests :
-	Tests techniques
-	Tests fonctionnels
-	Tests d’utilisabilité

## Tests techniques


Tests techniques : unitaires, non-régression, intégration…
Un test unitaire est la plus petite échelle de test possible. C’est un test automatisé concentré sur une portion de code précise en s’affranchissant de ses dépendances. Sa taille et sa durée d’exécution permettent une boucle de feedback rapide qui facilite la pratique du TDD.
Tests d’intégration
Test unitaires
(Mocha et JEst) 
•	Tests unitaires et tests d’acceptation : règles métiers
•	Test d’intégration : intégration avec un système tiers
•	Test de bout en bout : bonne collaboration de l’ensemble du système
Test de montée en charge
Test de performance
Tests de sécurité

## Tests fonctionnels


Tests fonctionnels
Tests de recette
## Tests d'utilisabilité


Tests d’utilisabilité
Session de tests utilisateurs en direct / vidéo. Tests supervisés qualitatifs pour validation et amélioration des développements réalisés.
L’objectif global du test d’utilisabilité est alors de mesurer la facilité d’utilisation de ce support
