---
home: false
footer: Vizeo-doc

---
# Présentation de SDS

Nous sommes [SDS](https://systragroup.sharepoint.com/sites/mysystra-group-idns/SitePages/Qeto.aspx), le studio de transformation digitale, data science et développement web qui fait partie de la direction digitale et innovation du groupe SYSTRA. 

Nous accompagnons les acteurs de la mobilité dans leur transformation digitale et dans la valorisation de leurs données.


# Démarche méthodologique générale pour le développement web

Nous adhérons à la [méthodologie agile](/approche-methodologique/principe-methode-agile.md) et à l’[approche UX design](/approche-methodologique/approche-ux-design.md) (User experience design). L'approche UX design place l’utilisateur final au cœur de la conception afin que le produit soit effectivement utile et utilisé. L’agile permet d’ajuster collaborativement la définition du rendu tout au long du projet afin que celui-ci réponde au mieux aux besoins du client et au budget disponible. 
Ces deux méthodologies requièrent un engagement fort de la part de l’équipe de production, mais également du responsable projet chez le client et des équipes du client, pour que les informations et les besoins circulent efficacement.

Notre méthodologie s'articule autour des étapes suivantes :
1. Cadrage du besoin et de la méthodologie agile
1. [Identification des besoins](/conception-orientee-utilisateurs/identification-des-besoins.md) 
2. [Co-construction](/conception-orientee-utilisateurs/co-construction.md)
3. [Spécification des besoins](/conception-orientee-utilisateurs/specification-des-besoins.md)
4. Développement itératif : UX/UI, [développement](/developpement-iteratif-agile/pratiques-de-developpement.md) et [test utilisateur](/developpement-iteratif-agile/pratiques-de-tests.md)
5. Application finale : [hébergement et maintenance de l'infrastructure](/application-finale/hebergement-et-maintenance-infrastructure.md), [support utilisateur et maintenance applicative](/application-finale/support-utilisateur-maintenance-applicative.md), [suivi et évaluation de l'expérience utilisateur](/application-finale/evaluation-suivi-UX.md) et [coordination/pilotage](/application-finale/coordination-pilotage.md)



![Schema méthodologie](./images/schema-methodologie-dev.png)
