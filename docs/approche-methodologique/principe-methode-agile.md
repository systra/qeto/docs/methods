# Principe de la méthode agile

Les méthodes agiles cadrent la gestion du projet de manière pragmatique. Elles reposent sur quatre valeurs fondamentales :
-	Les individus et leurs interactions plus que les processus et les outils ;
-	Des logiciels opérationnels plus qu'une documentation exhaustive ;
-	La collaboration avec les clients plus que la négociation contractuelle ;
-	L'adaptation au changement plus que le suivi d'un plan.

Nous appliquons le cadre scrum. Celui-ci reprend les valeurs agiles et crée un cadre de développement itératif. Une liste (backlog) de fonctionnalités destinées aux utilisateurs est d’abord établie avec les équipes techniques et le responsable de projet. À chaque itération, une partie de ce backlog est développée et livrée pour validation client.

![Cadre de développement scrum](../images/principe-methode-agile.png)
*Cadre de développement scrum*

Cette méthode permet de développer itérativement le produit et donc d’arriver rapidement à des premiers livrables testables par les futurs utilisateurs. Ce découpage permet également de se concentrer en priorité sur les fonctionnalités indispensables et assure donc qu’elles soient bien présentes dans le produit final.

La méthode scrum met l’accent sur les moyens mis à disposition et sur le pilotage par la valeur utilisateur. On ne cherche pas à développer toutes les fonctionnalités auxquelles on peut penser, mais plutôt à développer celles qui apportent le plus de valeur à l’utilisateur, quitte à y investir davantage et à passer moins de temps sur des fonctionnalités accessoires.
