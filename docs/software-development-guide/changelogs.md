# Keeping changelogs

[[TOC]]

## Generalities

### What is a changelog?
A changelog is a file that contains a curated, chronologically ordered list of notable changes for each version of a
project.

### Why keep a changelog?
To make it easier for users and contributors to see precisely what notable changes have been made between each release
(or version) of the project.

### Who needs a changelog?
People do. Whether consumers or developers, the end users of software are human beings who care about what's in the
software. When the software changes, people want to know why and how.

### How do I make a good changelog?
Try to stick to the principles described in https://keepachangelog.com/en/1.0.0/:
- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

## Changelogs in our projects
Depending on the way a project is set up, we have two different way to edit changelogs: the first consists of manually editing the `CHANGELOG.md` file, and the other consists of automated updates to the `CHANGELOG.md` file.

### Manual editing of the changelog
We track changes in a CHANGELOG.md file at the root of the repository.

We track the changes that have not yet been included in a [version tag](./sds-git-flow.md#versioning-and-tagging) in a
separate paragraph titled "Next".

### Automatic edition of the changelog
For projects that use the automatic [tagging CI pipeline](./tag-ci-task.md), new entries should be separated in various
files according to the following naming convention: `_CHANGELOGS/{Added, Fixed, Changed, Removed}/{branch_name}.md`.

The new entries will be added to `CHANGELOG.md` automatically (and the files in `_CHANGELOGS/` deleted) when the
[tag ci script](https://gitlab.com/systra/qeto/infra/gitlab-ci-templates/-/blob/master/tag-ci.yml) is run to create the
next version tag.

In this version, we support the following types of changes:
- `Added` for new features.
- `Fixed` for any bug fixes or security fixes.
- `Changed` for changes in existing functionality or refactoring.
- `Removed` for now removed features.
