# Creating version tags with CI job

[[toc]]

This document describes the usage of the Continuous Integration (CI) job that makes it possible to create version tags in one click.

If this is not set up for your repository, please refer to
[gitlab-ci-templates](https://gitlab.com/systra/qeto/infra/gitlab-ci-templates/-/blob/master/README.md) for instructions on setting up some of our standard CI tasks.

We use semantic versioning in our projects. Please read the [semantic versioning docs](https://semver.org/) before creating a tag.

Before creating a tag, make sure that all changes that you want to include in your new version have been merged to the target branch. The merge commit automatically launches a CI pipeline with the security job, and perhaps other jobs like automatic tests or linting.

From your repository's gitlab page, select `Build` -> `Pipelines`. You should find the pipeline that was launched by the merge commit. When you click on the pipeline, you will find a listing of all the CI jobs that were triggered by the merge commit. To the right of the jobs you should see three potential jobs: `tag:major`, `tag:minor` and `tag:patch`, like in the illustration below.

![View of the tag buttons](../images/ci-tag.png)

Depending on whether you need to create a major, minor or patch version, click on one of the buttons. The tag job will do the following:

- Create a new commit in which any files that record the project versions are updated (like `CHANGELOG.md`)
- Create a new git tag that is applied to the new commit

Note that if the `publish` CI task is configured for your repository, an image of the newly tagged version of your repository will also be pushed to the container repository.

