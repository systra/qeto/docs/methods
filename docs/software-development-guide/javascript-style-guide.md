# JavaScript Style guide

[[toc]]

## Introduction

This document is the initial basis for a JavaScript style guide for SDS.

This guide is meant to be a living document that can be modified or extended as we come up with questions or ambiguities that need clarification.

If you are developing with Vue.js, you can use this [.eslintrc.js configuration file](https://systragroup.sharepoint.com/sites/SYSTRADigital/Documents%20partages/Technique/06_M%C3%A9thodologies/01_Craftsmanship/Style_guides/eslintrc.vue.js).

##  Formatting

### Block indentation: +2 spaces

Each time a new block or block-like construct is opened, the indent increases by two spaces. When the block ends, the indent returns to the previous indent level. The indent level applies to both code and comments throughout the block. 

### Column limit: 120

JavaScript code has a column limit of 120 characters.

### Imports

Use [isort](https://pypi.org/project/isort/) to correctly order your imports.

## Language features

### Arrow functions

When the body of an arrow function is a single line, we do not add unnecessary brackets or return statement.

YES:

```code
      return this.contracts.filter(o => o._deleted !== true)
```

NO:

```code
      return this.contracts.filter(o => { return o._deleted !== true })
```

If the arrow function returns an object literal, however, the parser doesn't interpret the braces as an object literal, but as a block statement. 

The solution is to surround the brackets with parentheses

Example:

```code
    const newSteps = allSteps.map(o => ({
        label: o.label,
        type: o.type,
        days0: 0,
        daysX: 0,
    }))
```

### String literals

#### Use single quotes

Ordinary string literals are delimited with single quotes ( `'` ), rather than double quotes ( `"` ).

Exception: use double quotes ( `'` ) if the string contains a single quote ( `'` ).

For example:

```code
const steps = {
  Admissiblity: {
    id: 1,
    name: 'Recevabilité',
  },
  EvaluationSummary: {
    id: 2,
    name: "Dépôt d'avis",
  },
}
```

## Conventions

### Events and handlers

## Naming events

Give events simple names that describe what is happening. For example: `documentViewRequest`. Do not give them the name of what the listeners should do (for example `openDocument`; an `openDocument` would be emitted when a document is actually opened).

Do not postfix 'Event' or other redundant information to the name of the event.

## Naming handlers

Handlers begin with `on`. For example: `onDocumentViewRequest()`.

If the code in the handler must be called by other parts of code, extract the reused code into its own function. For example, the handler `onDocumentViewRequest` may call a method called `openDocument()`. No code should explicitly call `onDocumentViewRequest`.
