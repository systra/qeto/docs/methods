# Code reviews

[[toc]]

## Introduction

The present document describes the steps to follow when preparing and carryout out a code review. The steps are allocated to the *author* (the person who wrote the code) and to the *reviewer*.  

Whether you are the author or the reviewer, please always respect the *[Ten Commandments of Egoless Programming](https://blog.codinghorror.com/the-ten-commandments-of-egoless-programming/)*.

## Checklist before code review (*author*) 

[Back to top](#code-review-guide)

- **Hand-test your code**
    - Test all new functionalities
	- Test any pre-existing functionalities that may have been impacted by your modifications 
- **Reread** all of your modifications with a tool like [Gitk](https://www.atlassian.com/git/tutorials/gitk)
- **Clean up** your code: 
    - Follow the style guides that are available in the menu to the left.
    - Use a linter. The style guides have links to the pertinent linter configuration files. 
    - The code should be easy to understand: if it's not easy to understand, then it should probably be re-written. If you are tempted to write lots of comments, you should probably rewrite the code to have smaller functions with very explicit names. For the moment SDS doesn't have any best practice guides, but the first section of [Clean Code](https://systragroup.sharepoint.com/:b:/r/sites/cop-softwaredev/Documents/Reference%20Documents/Architecture%20and%20good%20practices/Clean%20Code.pdf?csf=1&web=1&e=uaRv5f) is a good start.
- **Remove unused code**: commented code, and any `console.log()` or `print()` used for debugging.
- **If the modifications have an impact on deployment or on the development environment, update the corresponding files.** Including, but not limited to: 
    - `README.md` 
    - the docker-compose configuration used for development
    - the create-release used to create the [Docker Compose Archive or DCA](https://gitlab.com/systra/qeto/infra/dca_format) and deploy the application
- **Unit tests** *if unit tests are required to consider that a story is done for your project*
    - Make sure that unit tests cover your code.
    - Verify that all unit tests pass.
- **Update the [`CHANGELOG.md`](https://keepachangelog.com/en/1.0.0/)**, *if there is one for your project*.
- **Run a security test** if a new library has been added to the project: `yarn audit`, `npm audit`, [safety](https://pypi.org/project/safety/) for Python, ...
- **Update migrations** if there has been a modification to the database structure.
- **Clean the git branch**: `git rebase -i origin/master` or equivalent.
    - Squash irrelevant commits on your branch. Often a single branch can be squashed into a single commit; this helps the project's history remain uncluttered. Maintain multiple commits if the functionalities implemented on them are clearly distinct.  
    - Make sure that the commit message is clear and specific for someone unfamiliar with the story. Please see [SDS Git Flow](sds-git-flow.md#commit-messages) for information on the title of commit messages.
- **Check the continuous integration tests**
    - If continuous integration (CI) is set up in the git repository, you can see whether all tests pass for your branch by selecting the view `Repository` -> `Branches`. A red `X` next to the branch means that it has failures. 
    - The details of the failure are recorded in the artifacts related to the test. To access the artifacts:
        - Click on the red `X` that corresponds to the branch.
        - Click on the `test` in the section `Pipeline`.
        - From the menu to the right, under `Job artifacts`, select `Browse`


## Launch the code review (*author*)    

[Back to top](#code-review-guide)

- Create a merge request (or several, if multiple repositories were modified in the implementation of your story) on [GitLab](https://gitlab.com/systra). 
    - If you want your code to be reviewed, but it is not yet done and ready for a merge, begin the name of the merge request with "Draft: ".
    - Add the code review checklist to the description of the merge request
      - A) If your repository has the code review checklist merge request template, then select it, OR
      - B) Copy [the code review checklist](pre-code-review-checklist.md) into the merge request description 
    - Add a description of your changes in the merge request description 
    - Go through the checklist
- Update the relevant issue(s) in [Jira](https://qeto.atlassian.net/secure/RapidBoard.jspa?rapidView=5&projectKey=SPRINTS)
    - Record time spent.
    - Change the status of the issue(s) to "CODE REVIEW".
- Contact the reviewer.

## Code review (*reviewer*)

[Back to top](#code-review-guide)


- Do not begin the code review until the author has copied the [code review checklist](pre-code-review-checklist.md) into the merge request description, and checked all relevant items. If some items are un-checked, you can discuss with the author to see if there is a good reason.
- It's often a good idea to start the review with a face-to-face or phone conversation between author and reviewer so that the author can present their modifications. The reviewer should also feel free to get back in touch with the author if any additional clarifications are needed. 
- Test the code
    - *Maybe the reviewer can not bother testing the code if the changes are simple and the author claims to have tested?* Otherwise: 
    - Run the unit tests to make sure they pass.
    - Make sure all functionalities (or bug corrections) have been implemented as described in the [Jira](https://qeto.atlassian.net/secure/RapidBoard.jspa?rapidView=5&projectKey=SPRINTS) issue. 
- Read the diffs in the merge request in GitLab and add comments. 
- Make sure that all the points listed above in [Preparing the code review](#preparing-the-code-review-author) have been addressed.

## Corrections (*author*)

[Back to top](#code-review-guide)

- Correct the code and push the corrections. 
- Respond to comments in the merge request as needed.
- *The author does not mark comments as "resolved"*.
- Let the reviewer know when everything as been addressed.

## Validation of corrections (*reviewer*)

[Back to top](#code-review-guide)

- Change to "resolved" any comments that have been satisfactorily corrected.
- Approve the merge request when it is ready to be merged.

## Finalising the merge (*author* and *reviewer*)

- Any points to which the author and the reviewer cannot agree or where they feel that there is ambiguity that should be discussed with the team should be noted and discussed in the Tech Hour or whatever ceremony is reserved for this. Currently, you can note such questions [here](https://teams.microsoft.com/l/file/4DE537CB-EF72-47F2-B94E-F75D56CAEB05?tenantId=f7e124e1-3cbb-4714-b90e-d08652874b65&fileType=docx&objectUrl=https%3A%2F%2Fsystragroup.sharepoint.com%2Fsites%2FSYSTRADigital%2FDocuments%20partages%2FTechnique%2F06_M%C3%A9thodologies%2F01_Craftsmanship%2FQuestions_venant_des_revues_de_code.docx&baseUrl=https%3A%2F%2Fsystragroup.sharepoint.com%2Fsites%2FSYSTRADigital&serviceName=teams&threadId=19:dd10c772032748f7b091cd10e3dca9e5@thread.skype&groupId=98cf44e8-b2df-4a23-9da3-8c49d64cec1a).

- If any modifications require specific actions in the dev environment to avoid breaking things, let the rest of the team know. (For example: need to  run `manage.py migrate` for a Django project, need to add a new API key to secrets, ...) 
- Merge can be done by the author or the reviewer.

[Back to top](#code-review-guide)
