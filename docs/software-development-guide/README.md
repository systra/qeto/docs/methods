# Getting started

[[toc]]

## Introduction

The documents in the Software Development Guide are written for developers and seek to help in the creation of consistent, high-quality, maintainable code.
This guide is currently under construction.

## Development environment - Linux

We develop in a Linux environment. 
SYSTRA's IT department has written up a guide to [Ubuntu Installation for developers](https://systragroup.sharepoint.com/sites/cop-softwaredev/Pages/ubuntu-installation.aspx). You don't necessarily need to follow all the steps, but do at least the following:
- Certificate installation
- Wifi configuration (if you are located in one of SYSTRA's offices) 
- System Upgrade
- VPN Installation

The instructions on software installation in the document above may be out of date, so use your favorite search engine to find the latest instructions.

You almost definitely want:
- docker: used by SDS for both development and on our integration and production environments
- git: version control, we use this for all projects
- keepass: for accessing project configurations and passwords

## Deployment and infrastructure

See the [Infrastructure documentation](https://gitlab.com/systra/qeto/docs/infra).

## Technical stack

Language and framework choices will vary over time, though at the moment we favor Python with the framework Django for the back, and Vue.js for the front. 


Our projects are typically composed of multiple repositories:
- frontend: a single page application
- backend: provides a REST or graphql API
- compose: configuration based on docker-compose for running the project for a local development environment
- create-release: script to create the [Docker Compose Archive](https://gitlab.com/systra/qeto/infra/dca_format) and to deploy the application 
