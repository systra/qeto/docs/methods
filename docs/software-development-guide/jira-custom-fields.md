# Jira custom fields

SDS uses a custom field for our Jira tickets: SDS Labels. This field should be filled out for each ticket, so that we can better track our work.

[[TOC]]

## SDS labels

These labels, in conjunction with the ticket type, help us see where we spend our time and what are the evolutions. For example, we can detect
that we spend more and more time supporting our users, and decide to improve our documentation.

Each ticket can have zero, one, or many of these labels:

### Coordination
Actions related to **organizing our work** or coordinating it with other teams.

::: tip Example
A ticket titled *"Specify a solution for adding comments to our application"*
:::

### Consultancy
Actions helping other teams of developers. That can be "doing something for them", "helping them do something" or "advise them".

::: tip Example
A ticket titled *"Do a Security update for a project we do not own"*, or *"Work in another team for a month"*, or *"Do an audit of a code we do not own"*.
:::

### Infra
Actions impacting how our **hosting infrastructure** works (addition of capabilities, configuration change), as opposed to an action regarding a single project.

::: tip Example
A ticket titled *"Configure CAdvisor to report metrics on our containers every 10 seconds"*
:::

### Maintenance
Refactoring, **updates**, security updates, documentation update.

::: tip Example
A ticket titled *"Add a healthcheck to the backend service of an application"*. 
:::

### Temps
Ongoing work that will never be "Done" as long as the concerned app or service is still running.

::: tip Example
A ticket titled *"Time spent for security updates"*, which will also get the label `Maintenance`,
or *"User support for app X"*, which will also get the label `Support`.
:::

### Support
**User support** :
- helping users outside our team use our applications,
- helping software teams use our hosting platform (helping them build the compose file, setting their rights with capp, setting up their CI ...)

::: tip Example
A ticket titled *"Help a user setup a project in an app"*

A ticket titled *"Help team A add a linter in their CI"*, or *"Authorize Team member A to access application logs for project B"*. This 
:::

### Unplanned
Action that wasn't planned in advance, that **we didn't anticipate a few days/weeks before**.

::: tip Example
A ticket titled *"Fix urgent bug"*, or *"Urgently implement security feature"*.
:::

