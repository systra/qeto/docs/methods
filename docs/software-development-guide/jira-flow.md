# Jira Flow

Our software development work is broken down into discrete items.
These items, commonly referred to as tickets or issues, describe the work to be done, the state of the work 
(to be refined, in progress, ...), and are also home to discussions about the work.
Our tickets are stored in the online tool Jira.

We have defined a 'Jira Flow' or workflow of states for the tickets. These states indicate, in a glance:
- The status of the ticket
- The next action on the ticket
- Who should do the next action

[[TOC]]

## Ticket states

### DONE
Nothing more is needed.

::: tip Next states & actions
There is no next state.

There is no one in charge of any action.
:::

### REJECTED
The ticket is not done and should not be done.

This happens for example for tickets that are duplicate another ticket, or tickets that we thought would be a good idea,
but for wich the context has changed, and they are no longer desirable.

::: tip Next states & actions
There is no next state.

There is no one in charge of any action.
:::

### FREEZE
We are unsure if the ticket should be done or not, and won't be able to tell for some time.

::: tip Next states & actions
The ticket can be 'unfrozen', but will need to be updated before it is ready to be implemented.
It can go to [SANDBOX](#sandbox), [BUSINESS INPUT](#business-input), [REFINEMENT](#refinement) or [REJECTED](#rejected).

Anyone can change the status of a frozen ticket; usually a ticket is unfrozen by the person that wants to revive it.
:::

### SANDBOX
The ticket is just a raw idea. Only its author is interested in it.

::: tip Next states & actions
To further detail the ticket, it can be moved to [BUSINESS INPUT](#business-input) or [REFINEMENT](#refinement).
Alternatively it can be [FREEZE](#freeze) or [REJECTED](#rejected).

Its author is in charge of changing its status from SANDBOX.
Usually, the author is the Product Owner (PO) or the Proxy Product Owner (Proxy PO)
:::

### BUSINESS INPUT
The idea is being discussed with users by the PO and/or Proxy PO, so we can understand its context and usefulness.

::: tip Next states & actions
Once we understand the business context, it can be moved to [REFINEMENT](#refinement),
[AWAITING ESTIMATION](#awaiting-estimation). Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The Product Owner or the Proxy-Product Owner are in charge of completing the business context.
:::

### REFINEMENT
The idea is being refined on the technical side, in a discussion involving all the team (developers and PO/Proxy PO). 

::: tip Next states & actions
Once we agree to the definition of the task at hand, it can be moved to [AWAITING ESTIMATION](#awaiting-estimation).
Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

Everyone in the team is in charge of making the task evolve. The comment area of the Jira ticket can be useful to keep
a log of what is expected from whom.
:::

### AWAITING ESTIMATION
The development team needs to estimate the time it will make to implement the ticket.
This will be an **estimation, not an engagement**. If the estimate is longer than 2 days, the ticket should be split in 
two or more smaller tickets.

::: tip Next states & actions
The ticket can be moved back to [REFINEMENT](#refinement) by the developers. This happens when additional discussion is
needed, either because the context is incomplete or because the time estimate is high, and the ticket needs to be split
into multiple tickets.

The ticket can be moved to [BACKLOG](#backlog) or [SELECTED FOR DEVELOPMENT](#selected-for-development) by the PO or
Proxy PO if the estimate is made and the task is small enough.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The developers are in charge of completing the estimation.
:::

### BACKLOG
The ticket is ready but of lesser priority than those in [SELECTED FOR DEVELOPMENT](#selected-for-development).

::: tip Next states & actions
The ticket can be moved back to [REFINEMENT](#refinement) by anyone if it sits in the backlog for too long (for example
more than a month), and we become unsure if the context has changed.

The ticket can be moved to [SELECTED FOR DEVELOPMENT](#selected-for-development) by the PO or Proxy PO if it is
prioritized, or by the developers if they lack work.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The PO and Proxy PO are in charge of prioritizing the tickets. Everyone is in charge of making sure we re-discuss older
tickets before implementing them.
:::

### SELECTED FOR DEVELOPMENT
Work is ready to be started by the developers.

::: tip Next states & actions
The ticket can be moved back to [REFINEMENT](#refinement) by anyone if too long time has passed since its refinement 
(for example more than a month), and we become unsure if the context has moved.

The ticket can be moved back to [BACKLOG](#backlog) by the PO or Proxy PO if the priorities have changed.

The ticket can be moved to [EN COURS](#en-cours) by the developer who takes its implementation in charge.

The ticket can be moved to [STANDBY](#standby) if it needs to wait a little for an asynchronous interaction (for 
example a question, the completion of another task that is being finalized, etc.).

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The PO and Proxy PO are in charge of prioritizing the tickets. Everyone is in charge of making sure we re-discuss older
tickets before implementing them. The developers are in charge of moving the ticket forward.
:::

### EN COURS
The implementation has begun but is not finished.

::: tip Next states & actions
The ticket can be moved to [CODE REVIEW](#code-review) by the developer who takes its implementation in charge.

The ticket can be moved to [STANDBY](#standby) if it is blocked and waiting for another action.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected) if priorities have changed.

The developer who is assigned to the ticket is in charge of moving the ticket forward.
:::

### STANDBY
The implementation is paused and waiting for an external event.

For example, the event can be a las minute detail needed to pursue the objective of the ticket, a conflict with another
task that is being finished, or the apocalypse.
::: tip Next states & actions
The ticket can be moved to [EN COURS](#en-cours) by the developer who takes its implementation in charge.

The ticket can be moved to any status that it was moved from.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The developers are in charge of prioritizing the tickets. Everyone is in charge of making sure we re-discuss older
tickets before implementing them. The developers are in charge of moving the ticket forward.
:::

### CODE REVIEW
The implementation is being reviewed by a peer. Please [see here for more details](code-reviews.md).
::: tip Next states & actions
The ticket can be moved back to [EN COURS](#en-cours) by a developer if there is a good amount of rework to do.

The ticket can be moved to [TO DEPLOY INTEG](#to-deploy-integ) or [TO DEPLOY STAGING](#to-deploy-staging) (deppending on
the project context) by a developer once the review is successful.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected).

The developers are in charge of moving the ticket forward.
:::
### TO DEPLOY INTEG
The ticket has to be deployed to the integration environment.

::: tip Next states & actions
The ticket can be moved to [FEEDBACK PROXY PO](#feedback-proxy-po) by a developer once the deployment is done.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected), bearing in mind it would likely translate in
some additional changes in the code.

The developers are in charge of moving the ticket forward.
:::
### FEEDBACK PROXY PO
The acceptance criteria of the ticket are being tested by the Proxy PO.

::: tip Next states & actions
The ticket can be moved to [TO DEPLOY STAGING](#to-deploy-staging) by the Proxy PO if the test is successful.

The ticket can be moved to [EN COURS](#en-cours) by the Proxy PO if the test is not successful, with instruction on what
fails in the comments.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected), bearing in mind it would likely translate in
some additional changes in the code.

The Proxy PO is in charge of moving the ticket forward.
:::

### TO DEPLOY STAGING
The ticket has to be deployed to the staging environment.

::: tip Next states & actions
The ticket can be moved to [FEEDBACK PO](#feedback-po) by a developer once the deployment is done.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected), bearing in mind it would likely translate in
some additional changes in the code.

The developers are in charge of moving the ticket forward.
:::

### FEEDBACK PO
The acceptance criteria of the ticket are being tested by the PO.

::: tip Next states & actions
The ticket can be moved to [TO DEPLOY PROD](#to-deploy-staging) by the PO if the test is successful.

The ticket can be moved to [EN COURS](#en-cours) by the PO if the test is not successful, with instruction on what
fails in the comments.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected), bearing in mind it would likely translate in
some additional changes in the code.

The Proxy PO is in charge of moving the ticket forward.
:::

### TO DEPLOY PROD
The ticket has to be deployed to the production environment.

::: tip Next states & actions
The ticket can be moved to [DONE](#done) by a developer once the deployment is done and if no user test is anticipated.

The ticket can be moved to [FEEDBACK USER](#feedback-user) by a developer once the deployment is done if a user test has
been anticipated for this ticket.

Alternatively, it can go to [FREEZE](#freeze) or [REJECTED](#rejected), bearing in mind it would likely translate in
some additional changes in the code.

The developers are in charge of moving the ticket forward.
:::

### FEEDBACK USER
Feedback is collected from the users. Seldom used.

::: tip Next states & actions
The ticket can be moved to [DONE](#done) by the PO once the feedback is gathered.

The PO is in charge of moving the ticket forward.
:::
