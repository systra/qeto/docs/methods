```
# Pre code review checklist

- **Hand-test your code**
    - [ ] Test all new functionalities
	- [ ] Test any pre-existing functionalities that may have been impacted by your modifications 
- [ ] **Reread** all of your modifications with a tool like [Gitk](https://www.atlassian.com/git/tutorials/gitk)
- **Clean up** your code: 
    - [ ] Follow the style guides that are available in the menu to the left.
    - [ ] Use a linter. The style guides have links to the pertinent linter configuration files. 
    - [ ] The code should be easy to understand: if it's not easy to understand, then it should probably be re-written. If you are tempted to write lots of comments, you should probably rewrite the code to have smaller functions with very explicit names. For the moment SDS doesn't have any best practice guides, but the first section of [Clean Code](https://systragroup.sharepoint.com/:b:/r/sites/cop-softwaredev/Documents/Reference%20Documents/Architecture%20and%20good%20practices/Clean%20Code.pdf?csf=1&web=1&e=uaRv5f) is a good start.
- [ ] **Remove unused code**: commented code, and any `console.log()` or `print()` used for debugging.
- **If the modifications have an impact on deployment or on the development environment, update the corresponding files.** Including, but not limited to: 
    - [ ] `README.md` 
    - [ ] the docker-compose configuration used for development
    - [ ] the create-release used to create the [Docker Compose Archive or DCA](https://gitlab.com/systra/qeto/infra/dca_format) and deploy the application
- **Unit tests** *if unit tests are required to consider that a story is done for your project*
    - [ ] Make sure that unit tests cover your code.
    - [ ] Verify that all unit tests pass.
- [ ] **Update the [`CHANGELOG.md`](https://keepachangelog.com/en/1.0.0/)**, *if there is one for your project*.
- [ ] **Run a security test** if a new library has been added to the project: `yarn audit`, `npm audit`, [safety](https://pypi.org/project/safety/) for Python, ...
- [ ] **Update migrations** if there has been a modification to the database structure.
- **Clean the git branch**: `git rebase -i origin/master` or equivalent.
    - [ ] Squash irrelevant commits on your branch. Often a single branch can be squashed into a single commit; this helps the project's history remain uncluttered. Maintain multiple commits if the functionalities implemented on them are clearly distinct.  
    - [ ] Make sure that the commit message is clear and specific for someone unfamiliar with the story. Please see [SDS Git Flow](sds-git-flow.md#commit-messages) for information on the title of commit messages.
- [ ] **Check the continuous integration tests**
    - If continuous integration (CI) is set up in the git repository, you can see whether all tests pass for your branch by selecting the view `Repository` -> `Branches`. A red `X` next to the branch means that it has failures. 
    - The details of the failure are recorded in the artifacts related to the test. To access the artifacts:
        - Click on the red `X` that corresponds to the branch.
        - Click on the `test` in the section `Pipeline`.
        - From the menu to the right, under `Job artifacts`, select `Browse`
- [ ] Add a clear description of your modifications to the merge request
```
