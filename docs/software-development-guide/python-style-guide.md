# Python style guide

[[toc]]

## Introduction

This document is the initial basis for a Python style guide for SDS.

This guide is meant to be a living document that can be modified or extended as we come up with questions or ambiguities that need clarification.

In general, refer to the style guidelines for Python defined in [PEP 8](https://www.python.org/dev/peps/pep-0008/).

Flake8 is a tool for style guide enforcement. Please use it. See the [sample .flake8 configuration file](https://systragroup.sharepoint.com/sites/SYSTRADigital/Documents%20partages/Technique/06_M%C3%A9thodologies/01_Craftsmanship/Style_guides/flake8)

##  Formatting

### Column limit: 120

Python code has a column limit of 120 characters.

### Whitespace

We make an exception to PEP8 by preferring whitespace around all operators, regardless of priority. We find this approach to be both readable and consistent with rules applied in JavaScript.

YES:

```code
END_OF_DAY = 3600 * 24 - 1
FOO = (1 + 2) * 3
```

NO:

```code
END_OF_DAY = 3600*24 - 1
FOO = (1+2) * 3
```

## Language features

### String literals

#### Use single quotes

Ordinary string literals are delimited with single quotes ( `'` ), rather than double quotes ( `"` ).

Exception: double quotes ( `"` ) may be used if the string contains a single quote ( `'` ).

For example:

```code
    users = List(cls, part_name=String(description='Au moins trois lettres'), description="Utilisateurs d'Azure")
    name = 'all_azure_users'
```







