# SDS Git Flow and Versioning

[[toc]]

## Branching

As much as possible, each new story or bug has its own branch.

A code review should be carried out as soon as possible once the story or bug fix has been completed. Immediately following the code review, the branch must be merged back to the main trunk (usually called master or dev).

The feature branch must be rebased before merging if necessary. **Non** fast-forward merges are used, in order to better see the related commits in the git history.

The branches are named using the following convention: `<ticket ID>_<title>`

* `<ticket ID>`: the ID of the ticket in Jira, our ticketing tool. This ID is written in uppercase. For example: `VIS-101`,
* `<title>`: a short description of the work involved, with spaces replaced by underscores. For example: `add_internal_notes`

An example name of the branch could be: `VIS-101_add_internal_notes`

Exceptions are possible for hot fixes committed directly on the trunk: in this case, no additional branch is created.

## Commit messages

Commit message titles must begin with their `ticket ID` in uppercase followed by `: ` and a short description.
For example, a commit message title could be: `VIS-101: add internal notes`.

The commit message body can further explain what has been done in the commit. The body is separated from the title with an empty line.

For example, a full commit message could be:
```
VIS-101: add internal notes

- Added
  - query allInternalNotes(documentVersion_Id: "xxx")
  - 'internalNotes' field to DocumentVersionNode

```

## Versioning and Tagging

Tagging is done using [semantic versioning](https://semver.org/). A 'v' is used as a prefix in the name of the tag. For example, a tag name can be `v0.2.1`.

For many projects, tagging can be done in a single click using the tag Continuous Integration (CI) task. For more information on this, please see [Creating version tags with CI job](./tag-ci-task.md) 

If your project is not set up with the tag CI, you can create the tag manually. A tag should be accompanied by a message repeating the version number, and can contain a short explanation of the changes. However this explanation is usually unnecessary as it is already available in the changelog. Tags must be annotated, showing when and by whom the tag has been created.

For example:

* Tag name: `v0.1.2`
* Tag message: `version 0.1.2`
* Example command: `git tag -a v0.1.2 -m 'version 0.1.2'`


## Deployment

Branches must be merged and tagged before deployment on `prod` and `staging` environments.

Merging branches onto the main branch (let's call it master) before deploying them to staging helps to avoid a bottleneck in the case that staged features are not validated quickly. In any case, deciding which branch should be merged and validated first would be complex and would not add value.

Imagine that a feature F has been merged to master, tagged and deployed to staging. Then another feature, F+1, is reviewed. F+1's branch can be merged to master, and master can be tagged and redeployed to staging, even if F has not yet been validated, because the latest version of master contains both F and F+1.
